# VPC Random Example
---
## **Docker Implementation**
---
### **Docker installation**
1. Install [docker-compose](https://docs.docker.com/compose/install/).

    Verify `docker-compose` installation:
    ```bash
    docker-compose --version
    ```
2. Get your `AWS_ACCESS_KEY_ID` and `AWS_ACCESS_SECRET_KEY` from your AWS user account.

    Guide: [How to create AWS access keys](https://aws.amazon.com/premiumsupport/knowledge-center/create-access-key/).

### **Docker execution**
1. Clone this repository using [Git](https://git-scm.com/).
2. Change to the `working directory`.
    
    ```bash
    cd terraform-aws-vpc-example
    ```
3. Initialize `terraform aws provider` on the `docker image` using `terraform init`.
    ```bash
    docker-compose run \
        -e AWS_ACCESS_KEY_ID={YOUR_AWS_ACCESS_KEY_ID} \
        -e AWS_SECRET_ACCESS_KEY={AWS_SECRET_ACCESS_KEY} \
        --rm tf init
    ```
4. Run the `terraform plan` and review the changes to commit on your AWS account.
    ```bash
    docker-compose run \
        -e AWS_ACCESS_KEY_ID={YOUR_AWS_ACCESS_KEY_ID} \
        -e AWS_SECRET_ACCESS_KEY={AWS_SECRET_ACCESS_KEY} \
        --rm tf plan
    ```
5. Run the `terraform plan -auto-approve` to commit the changes to your AWS account.
    ```bash
    docker-compose run \
        -e AWS_ACCESS_KEY_ID={YOUR_AWS_ACCESS_KEY_ID} \
        -e AWS_SECRET_ACCESS_KEY={AWS_SECRET_ACCESS_KEY} \
        --rm tf apply -auto-approve
    ```
6. (Optional ) Remove everything you just deployed using `terraform destroy`.
    ```bash
    docker-compose run \
        -e AWS_ACCESS_KEY_ID={YOUR_AWS_ACCESS_KEY_ID} \
        -e AWS_SECRET_ACCESS_KEY={AWS_SECRET_ACCESS_KEY} \
        --rm tf destroy -auto-approve
    ```
* **Notes:**
    * These commands will work if your AWS user account have the `right permissions`.
    * For `steps 3, 4, 5 and 6`, change `{YOUR_AWS_ACCESS_KEY_ID}` and `{AWS_SECRET_ACCESS_KEY}` with the one you obtained from the previous steps *(`without the brackets`)*.
---
## **VPC Details**
---
## AWS Cloud Diagram
![AWS Cloud Diagram](/images/aws-vpc.png)
## VPC -  `my-test-vpc`

| CIDR      | `10.0.0.0/26` |
| ----------| ----------- |
| Region       | `us-east-2`             
| Total IP     | `32 - 26 = 6 \| 2^6 = 64 IPs` |
| Network IP   | `10.0.0.0` |
| Broadcast IP | `10.0.0.63` |
| Usable IP    | `10.0.0.1 - 10.0.0.62` |    

## SUBNETS
* ### `public1_subnet`
    | CIDR      | `10.0.0.0/28` |
    | ----------| ----------- |
    | Total IP     | `32 - 28 = 4 \| 2^4 = 16 IPs` |
    | Network IP   | `10.0.0.0` |
    | Broadcast IP | `10.0.0.15` |
    | Usable IP    | `10.0.0.1 - 10.0.0.14` |
    | Availability Zone | `us-east-2a` |
    ---
* ### `public2_subnet`
    | CIDR      | `10.0.0.16/28` |
    | ----------| ----------- |
    | Total IP     | `32 - 28 = 4 \| 2^4 = 16 IPs` |
    | Network IP   | `10.0.0.16` |
    | Broadcast IP | `10.0.0.31` |
    | Usable IP    | `10.0.0.17 - 10.0.0.30` |
    | Availability Zone | `us-east-2b` |
    ---
* ### `private3_subnet`
    | CIDR      | `10.0.0.32/28` |
    | ----------| ----------- |
    | Total IP     | `32 - 28 = 4 \| 2^4 = 16 IPs` |
    | Network IP   | `10.0.0.32` |
    | Broadcast IP | `10.0.0.47` |
    | Usable IP    | `10.0.0.33 - 10.0.0.46` |
    | Availability Zone | `us-east-2a` |   
    ---
* ### `private4_subnet`
    | CIDR      | `10.0.0.48/28` |
    | ----------| ----------- |
    | Total IP     | `32 - 28 = 4 \| 2^4 = 16 IPs` |
    | Network IP   | `10.0.0.48` |
    | Broadcast IP | `10.0.0.63` |
    | Usable IP    | `10.0.0.49 - 10.0.0.62` |
    | Availability Zone | `us-east-2b` |


## Internet Gateway -  `my-test-igw`
* ### Attach VPC via the AWS console or...
* ### Attach IGW to VPC thru (command-line):
    * `AWS CLI Windows PowerShell`
    ```powershell
    aws ec2 attach-internet-gateway --vpc-id \"{vpc_id}\" --internet-gateway-id \"{igw-id}\" --region us-east-2   
    ```
    * `AWS CLI Linux`
    ```bash
    aws ec2 attach-internet-gateway --vpc-id "{vpc_id}" --internet-gateway-id "{igw-id}" --region us-east-2
    ```

## Custom Route Table - `my-test-public-rt`
1. Add route `0.0.0.0/0 (Destination)` to `my-test-igw (Target)`.
2. Add `public1_subnet` and `public2_subnet` to `Subnet Associations`.
