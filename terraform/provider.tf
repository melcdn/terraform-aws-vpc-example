provider "aws" {
  region = "us-east-2"
}

# Query available AZs
data "aws_availability_zones" "available" {}