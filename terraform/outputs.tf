output "public_subnets" {
  value = aws_subnet.public_subnet.*.id
}

output "security_group" {
  value = aws_security_group.sg.id
}

output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "public1_subnet" {
  value = element(aws_subnet.public_subnet.*.id, 1)
}

output "public2_subnet" {
  value = element(aws_subnet.public_subnet.*.id, 2)
}

output "private1_subnet" {
  value = element(aws_subnet.private_subnet.*.id, 1)
}

output "private2_subnet" {
  value = element(aws_subnet.private_subnet.*.id, 2)
}
