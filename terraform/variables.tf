variable "vpc_cidr" {
  default = "10.0.0.0/26"
}

variable "public_cidrs" {
  type    = list(string)
  default = ["10.0.0.0/28", "10.0.0.16/28"]
}

variable "private_cidrs" {
  type    = list(string)
  default = ["10.0.0.32/28", "10.0.0.48/28"]
}